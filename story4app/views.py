from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Schedule_Form
from .models import Schedule


def home(request):
	return render(request, 'home.html')

def aboutwriter(request):
	return render(request, 'aboutwriter.html')

def contact(request):
	return render(request, 'contact.html')

def favorite(request):
	return render(request, 'favorite.html')

def guestbook(request):
	return render(request, 'guestbook.html')
response = {}
def schedule(request):
	response['schedule_form'] = Schedule_Form
	return render(request, 'schedule.html', response)

def schedule_post(request):
	form = Schedule_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['activity'] = request.POST['activity']
		response['day'] = request.POST['day']
		response['date'] = request.POST['date']
		response['time'] = request.POST['time']
		response['place'] = request.POST['place'] if request.POST['place'] != "" else "-"
		response['category'] = request.POST['category'] if request.POST['category'] != "" else "-"
		schedule = Schedule(activity=response['activity'], day=response['day'], date=response['date'], time=response['time'],
                          place=response['place'], category=response['category'])
		schedule.save()
		return HttpResponseRedirect('/result/')
	else:
		return HttpResponseRedirect('/schedule/')

def result(request):
    schedule = Schedule.objects.all()
    response['schedule'] = schedule
    return render(request, 'result.html' , response)

def remove_items(request):
	delete = Schedule.objects.all().delete()
	return HttpResponseRedirect('/result/')