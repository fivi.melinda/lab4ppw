from django import forms
class Schedule_Form(forms.Form):

    error_messages = {
        'required': 'Please fill this field',
    }
    attrs = {
        'class': 'form-control',
        'type' : 'text',
    }
    date_attrs = {
        'class': 'form-control',
        'type' : 'date'
    }
    time_attrs = {
        'class': 'form-control',
        'type': 'time'
    }


    activity = forms.CharField(label='Activity', required=True, max_length=30, widget=forms.TextInput(attrs=attrs))
    day = forms.CharField(label='Day', required=False, max_length=30, widget=forms.TextInput(attrs=attrs))
    date = forms.DateField(label='Date', required=True, widget=forms.DateInput(attrs=date_attrs))
    time = forms.TimeField(label='Time', required=True, widget=forms.TimeInput(attrs=time_attrs))
    place = forms.CharField(label='Place', required=False, max_length=30, widget=forms.TextInput(attrs=attrs))
    category = forms.CharField(label='Category', required=False, max_length=30, widget=forms.TextInput(attrs=attrs))
