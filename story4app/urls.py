from django.urls import path
from . import views

urlpatterns = [
 path(r'', views.home, name='home'),
 path(r'aboutwriter/', views.aboutwriter, name='aboutwriter'),
 path(r'contact/', views.contact, name='contact'),
 path(r'favorite/', views.favorite, name='favorite'),
 path(r'guestbook/', views.guestbook, name='guestbook'),
 path(r'schedule/',views.schedule, name='schedule'),
 path(r'result/', views.result, name='result'),
 path(r'schedule_post/', views.schedule_post, name='schedule_post'),
 path(r'remove_items/', views.remove_items, name="remove_items"),
]
